# tryst

An experiment in real-time P2P physics games in the browser.

## Building/running

Requires all of the following in your `$PATH`:

- [`bash`][bash].
- [`getopt`](https://en.wikipedia.org/wiki/Getopt#In_Shell) (modern
  implementation from [`util-linux`](https://en.wikipedia.org/wiki/Util-linux);
  not to be confused with the related
  [`getopts`](https://en.wikipedia.org/wiki/Getopts)).
- [`cargo`](https://rustup.rs/) (including the
  [rustfmt](https://github.com/rust-lang/rustfmt) component).
- [`npm`](https://nodejs.org/) (try [`fnm`](https://github.com/Schniz/fnm),
  [`n`](https://github.com/tj/n), or [`nvm`](https://github.com/nvm-sh/nvm)).
- [`wasm-bindgen`](https://rustwasm.github.io/wasm-bindgen/).
- [`wasm-opt`](https://github.com/WebAssembly/binaryen); for release builds
  (specifying `-O`/`--release`) only.

### Building

```bash
git clone https://codeberg.org/nullitopy/tryst.git
cd tryst
./build.sh -O  # Or `./build.sh` for a debug/development build. For a full
               # listing of command-line usage, run `./build.sh -h`.
```

### Running

```bash
cd out
python -m http.server  # Or any HTTP(S) server that correctly serves WASM with
                       # the `application/wasm` MIME type.
firefox http://localhost:8000/
```

[bash]: https://en.wikipedia.org/wiki/Bash_(Unix_shell)
