#version 300 es

precision mediump float;

out vec4 outColor;

void main() {
    outColor = vec4(0.9, 0.6, 0.3, 1.0);
}
