#version 300 es

precision mediump float;

in vec2 a_position;

uniform vec2 u_resolution;
uniform vec2 u_translation;
uniform vec2 u_rotation;

void main() {
    vec2 rotated = vec2(
        a_position.x * u_rotation.x - a_position.y * u_rotation.y,
        a_position.y * u_rotation.x + a_position.x * u_rotation.y
    );
    vec2 translated = rotated + u_translation;
    vec2 normalized = translated / u_resolution;
    vec2 clipSpace = 2.0 * normalized;
    vec2 scaled = 32.0 * clipSpace;

    gl_Position = vec4(scaled, 0, 1);
}
