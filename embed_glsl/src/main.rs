#![forbid(unsafe_code)]

use std::{
    error::Error,
    fs::{self, File},
    io::{Read, Write},
};

fn main() -> Result<(), Box<dyn Error>> {
    let mut buf = String::new();
    for de in fs::read_dir("./glsl/")? {
        if let Ok(de) = de {
            let path = de.path();
            if path.extension().and_then(|os| os.to_str()) == Some("glsl") {
                let mut f = File::open(&path)?;
                buf.push_str("export const ");
                buf.push_str(
                    path.file_stem().and_then(|os| os.to_str()).unwrap(),
                );
                buf.push_str(" = `");
                f.read_to_string(&mut buf)?;
                buf.push_str("`;\n\n");
            }
        }
    }

    let mut output_file = File::create("./ts/glsl.ts")?;
    output_file.write_all(buf.as_bytes())?;

    Ok(())
}
