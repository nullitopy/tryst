#![forbid(unsafe_code)]

pub mod export_types;
mod player_input;

use export_types::PhysicsOutput;
use player_input::AbstractKey;
use rapier2d::prelude::*;
use std::cell::RefCell;
use wasm_bindgen::prelude::*;

const GRAVITY_MAGNITUDE: f32 = 32.0;
const JUMP_MAGNITUDE: f32 = 24.0;
const PLAYER_FORCE_MAGNITUDE: f32 = 48.0;

struct PhysicsState {
    physics_pipeline: PhysicsPipeline,
    gravity: Vector<Real>,
    integration_parameters: IntegrationParameters,
    island_manager: IslandManager,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    rigid_body_set: RigidBodySet,
    collider_set: ColliderSet,
    impulse_joint_set: ImpulseJointSet,
    multibody_joint_set: MultibodyJointSet,
    ccd_solver: CCDSolver,
    rigid_body_handles: RigidBodyHandles,
}

struct RigidBodyHandles {
    player: RigidBodyHandle,
}

thread_local! {
    static PHYSICS_STATE: RefCell<Option<PhysicsState>> = RefCell::new(None);
}

/// Must be called before ever calling `physics_step`. Must be called exactly
/// once.
#[wasm_bindgen]
pub fn physics_setup() {
    let mut rigid_body_set = RigidBodySet::new();
    let mut collider_set = ColliderSet::new();

    // Create the ground.
    let collider = ColliderBuilder::halfspace(Vector::y_axis()).build();
    collider_set.insert(collider);

    // Create the player.
    let player_body = RigidBodyBuilder::dynamic()
        .translation(vector![0.0, 9.0])
        .rotation(0.127_287_889_728_924_38)
        .linear_damping(1.0 / 2.0)
        .angular_damping(1.0 / 8.0)
        .build();
    let player_collider = ColliderBuilder::capsule_y(0.5, 0.5)
        .restitution(0.5)
        .friction(1.0)
        .friction_combine_rule(CoefficientCombineRule::Average)
        .build();
    let player_body_handle = rigid_body_set.insert(player_body);
    collider_set.insert_with_parent(
        player_collider,
        player_body_handle,
        &mut rigid_body_set,
    );

    // Create other structures necessary for the simulation.
    let gravity = vector![0.0, -GRAVITY_MAGNITUDE];
    let integration_parameters = IntegrationParameters {
        dt: 1.0 / 60.0,
        ..Default::default()
    };
    let physics_pipeline = PhysicsPipeline::new();
    let island_manager = IslandManager::new();
    let broad_phase = BroadPhase::new();
    let narrow_phase = NarrowPhase::new();
    let impulse_joint_set = ImpulseJointSet::new();
    let multibody_joint_set = MultibodyJointSet::new();
    let ccd_solver = CCDSolver::new();

    let rigid_body_handles = RigidBodyHandles {
        player: player_body_handle,
    };

    let physics_state = PhysicsState {
        physics_pipeline,
        gravity,
        integration_parameters,
        island_manager,
        broad_phase,
        narrow_phase,
        rigid_body_set,
        collider_set,
        impulse_joint_set,
        multibody_joint_set,
        ccd_solver,
        rigid_body_handles,
    };

    PHYSICS_STATE.with(move |ps| {
        ps.replace(Some(physics_state));
    });
}

#[wasm_bindgen]
pub fn physics_step(physics_output: &mut PhysicsOutput, keyboard_state: u8) {
    PHYSICS_STATE.with(|physics_state| {
        let mut ps_inner = physics_state.borrow_mut();
        let ps = ps_inner.as_mut().unwrap();

        let player_body = &mut ps.rigid_body_set[ps.rigid_body_handles.player];
        player_body.reset_forces(false);
        let jumping = keyboard_state & u8::from(AbstractKey::Jump) != 0;
        let jumped = match (
            keyboard_state & u8::from(AbstractKey::Left) != 0,
            keyboard_state & u8::from(AbstractKey::Right) != 0,
        ) {
            (true, false) => {
                player_body
                    .add_force(vector![-PLAYER_FORCE_MAGNITUDE, 0.0], true);

                if jumping {
                    player_body.apply_impulse(vector![0.0, -4.0], false);
                    player_body.apply_torque_impulse(JUMP_MAGNITUDE, false);

                    true
                } else {
                    false
                }
            }
            (false, true) => {
                player_body
                    .add_force(vector![PLAYER_FORCE_MAGNITUDE, 0.0], true);

                if jumping {
                    player_body.apply_impulse(vector![0.0, -4.0], false);
                    player_body.apply_torque_impulse(-JUMP_MAGNITUDE, false);

                    true
                } else {
                    false
                }
            }
            _ => false,
        };

        ps.physics_pipeline.step(
            &ps.gravity,
            &ps.integration_parameters,
            &mut ps.island_manager,
            &mut ps.broad_phase,
            &mut ps.narrow_phase,
            &mut ps.rigid_body_set,
            &mut ps.collider_set,
            &mut ps.impulse_joint_set,
            &mut ps.multibody_joint_set,
            &mut ps.ccd_solver,
            &(),
            &(),
        );

        let player_body = &ps.rigid_body_set[ps.rigid_body_handles.player];
        let pos = player_body.translation();
        let rot = player_body.rotation();

        physics_output.player_transform.x = pos.x;
        physics_output.player_transform.y = pos.y;
        physics_output.player_transform.re = rot.re;
        physics_output.player_transform.im = rot.im;
        physics_output.jumped = jumped;
    })
}
