use wasm_bindgen::prelude::*;

#[wasm_bindgen(getter_with_clone)]
pub struct PhysicsOutput {
    pub player_transform: Transform,
    pub jumped: bool,
}

#[wasm_bindgen]
#[derive(Clone)]
pub struct Transform {
    pub x: f32,
    pub y: f32,
    pub re: f32,
    pub im: f32,
}

#[wasm_bindgen]
impl PhysicsOutput {
    pub fn default() -> Self {
        Self {
            player_transform: Transform::default(),
            jumped: false,
        }
    }
}

#[wasm_bindgen]
impl Transform {
    #[wasm_bindgen(constructor)]
    pub fn new(x: f32, y: f32, re: f32, im: f32) -> Self {
        Self { x, y, re, im }
    }

    pub fn default() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            re: 1.0,
            im: 0.0,
        }
    }
}
