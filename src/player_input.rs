#[repr(u8)]
pub enum AbstractKey {
    Left = 0b001,
    Right = 0b010,
    Jump = 0b100,
}

impl From<AbstractKey> for u8 {
    fn from(ak: AbstractKey) -> Self {
        ak as Self
    }
}
