import { graphicsSetup, GraphicsState } from "./graphics.js";
import * as physics from "./physics.js";
import { AbstractKey, JUMP_INTERVAL, startListening } from "./playerInput.js";
import init, { PhysicsOutput } from "./tryst.js";
import * as wasm from "./tryst.js";

function mainLoop(
    gfxState: GraphicsState,
    getKeyboardState: () => number,
): FrameRequestCallback {
    let prevTimeStamp = -1;
    let dtAccumulator = 0.0;
    const physicsOutput = PhysicsOutput.default();
    let timeSinceLastJump = 0.0;
    function loop(timeStamp: number): void {
        window.requestAnimationFrame(loop);

        const firstLoop = prevTimeStamp === -1;
        const dt = timeStamp - prevTimeStamp;
        prevTimeStamp = timeStamp;
        if (firstLoop) {
            return;
        }

        // Player inputs...
        timeSinceLastJump += dt;
        timeSinceLastJump = Math.min(timeSinceLastJump, JUMP_INTERVAL);
        let effectiveKeyboardState =
            getKeyboardState() &
            (timeSinceLastJump === JUMP_INTERVAL ? ~0b000 : ~AbstractKey.Jump);

        // Physics...
        let didPhysicsStep = false;
        dtAccumulator += dt;
        while (dtAccumulator >= physics.TIMESTEP) {
            dtAccumulator -= physics.TIMESTEP;
            wasm.physics_step(physicsOutput, effectiveKeyboardState);
            didPhysicsStep = true;

            if (physicsOutput.jumped) {
                timeSinceLastJump = 0.0;
                effectiveKeyboardState &= ~AbstractKey.Jump;
            }
        }

        // Graphics...
        if (didPhysicsStep) {
            gfxState.drawFrame(physicsOutput.player_transform);
        }
    }

    return loop;
}

function main(): void {
    wasm.physics_setup();
    window.requestAnimationFrame(mainLoop(graphicsSetup(), startListening()));
}

function mainInit(): void {
    init().then(main).catch(console.error);
}

document.addEventListener("DOMContentLoaded", mainInit);
