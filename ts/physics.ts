/**
 * In milliseconds (ms).
 */
export const TIMESTEP = 1_000.0 / 60.0;
