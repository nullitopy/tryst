import { createProgram } from "./gl.js";
import * as glsl from "./glsl.js";
import { Transform } from "./tryst.js";

/**
 * The number of triangles to use to draw a quarter-circle.
 */
const QUARTER_CIRCLE_RES = 5;
/**
 * The number of vertices used in drawing a single capsule.
 */
const CAPSULE_VERT_COUNT = 4 * QUARTER_CIRCLE_RES + 2;

const VERTS = (() => {
    const verts = new Float32Array(8 * QUARTER_CIRCLE_RES + 12);

    // Top cap.

    [verts[0], verts[1]] = [0.0, 1.0];
    let ix = 2;

    /*
    See also: <https://stackoverflow.com/a/26570695>,
    <https://en.wikipedia.org/wiki/Sine_and_cosine#Double_angle_formulas>.

     r*cos(2t) = cos(t)*( r*cos(t)) - sin(t)*( r*sin(t) + h) + h*sin(t)
    -r*cos(2t) = cos(t)*(-r*cos(t)) - sin(t)*(-r*sin(t) - h) - h*sin(t)

     r*sin(2t)     = sin(t)*( r*cos(t)) + cos(t)*( r*sin(t)    )
     r*sin(2t) + h = sin(t)*( r*cos(t)) + cos(t)*( r*sin(t) + h) - h*(cos(t)-1)
    -r*sin(2t) - h = sin(t)*(-r*cos(t)) + cos(t)*(-r*sin(t) - h) + h*(cos(t)-1)
    */
    const theta = Math.PI / 2.0 / QUARTER_CIRCLE_RES;
    const [cosTheta, sinTheta] = [Math.cos(theta), Math.sin(theta)];
    let [x, y] = [0.0, 1.0];
    for (let i = 1; i < QUARTER_CIRCLE_RES; ++i) {
        const xNext = cosTheta * x - sinTheta * y + 0.5 * sinTheta;
        y = sinTheta * x + cosTheta * y - 0.5 * (cosTheta - 1.0);
        x = xNext;

        verts[ix++] = x;
        verts[ix++] = y;

        verts[ix++] = -x;
        verts[ix++] = y;
    }

    verts[ix++] = -0.5;
    verts[ix++] = 0.5;

    verts[ix++] = 0.5;
    verts[ix++] = 0.5;

    // Rectangular body.

    verts[ix++] = -0.5;
    verts[ix++] = -0.5;

    verts[ix++] = 0.5;
    verts[ix++] = -0.5;

    // Bottom cap.

    [x, y] = [-0.5, -0.5];
    for (let i = 1; i < QUARTER_CIRCLE_RES; ++i) {
        const xNext = cosTheta * x - sinTheta * y - 0.5 * sinTheta;
        y = sinTheta * x + cosTheta * y + 0.5 * (cosTheta - 1.0);
        x = xNext;

        verts[ix++] = x;
        verts[ix++] = y;

        verts[ix++] = -x;
        verts[ix++] = y;
    }

    verts[ix++] = 0.0;
    verts[ix++] = -1.0;

    // Ground.

    verts.set([24.0, -12.0, 24.0, 0.0, -24.0, -12.0, -24.0, 0.0], ix);

    return verts;
})();

export class GraphicsState {
    constructor(
        public gl: WebGL2RenderingContext,
        public testProgram: GraphicsProgram<Bufs, Unifs>,
    ) {}

    public drawFrame(playerTransform: Transform): void {
        const gl = this.gl;

        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clearColor(0.3, 0.32, 0.33, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);

        gl.useProgram(this.testProgram.prog);
        gl.bindVertexArray(this.testProgram.vao);

        gl.uniform2f(
            this.testProgram.unifs.u_resolution,
            gl.canvas.width,
            gl.canvas.height,
        );

        gl.uniform2f(
            this.testProgram.unifs.u_translation,
            playerTransform.x,
            playerTransform.y,
        );
        gl.uniform2f(
            this.testProgram.unifs.u_rotation,
            playerTransform.re,
            playerTransform.im,
        );
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, CAPSULE_VERT_COUNT);

        gl.uniform2f(this.testProgram.unifs.u_translation, 0.0, 0.0);
        gl.uniform2f(this.testProgram.unifs.u_rotation, 1.0, 0.0);
        gl.drawArrays(gl.TRIANGLE_STRIP, CAPSULE_VERT_COUNT, 4);
    }
}

class GraphicsProgram<B, U> {
    constructor(
        public prog: WebGLProgram,
        public vao: WebGLVertexArrayObject,
        public attrBufs: B,
        public unifs: U,
    ) {}
}

class Bufs {
    constructor(public a_position: WebGLBuffer) {}
}

class Unifs {
    constructor(
        public u_resolution: WebGLUniformLocation,
        public u_translation: WebGLUniformLocation,
        public u_rotation: WebGLUniformLocation,
    ) {}
}

export function graphicsSetup(): GraphicsState {
    const canvas = document.getElementById("canvas") as HTMLCanvasElement;
    const gl = canvas.getContext("webgl2");
    if (gl === null) {
        throw Error(
            "WebGL 2 is not supported by this browser & browser \
configuration. If you\u{2019}re using Firefox, ensure that \
`webgl.enable-webgl2` is set to `true` and that `webgl.disabled` is set to \
`false` in your `about:config`.",
        );
    }

    const prog = createProgram(gl, glsl.test_vert, glsl.test_frag);
    const a_positionLoc = gl.getAttribLocation(prog, "a_position");
    const u_resolutionLoc = gl.getUniformLocation(prog, "u_resolution");
    const u_translationLoc = gl.getUniformLocation(prog, "u_translation");
    const u_rotationLoc = gl.getUniformLocation(prog, "u_rotation");
    if (
        u_resolutionLoc === null ||
        u_translationLoc === null ||
        u_rotationLoc === null
    ) {
        throw Error("Could not locate uniform.");
    }
    const a_positionBuf = gl.createBuffer();
    if (a_positionBuf === null) {
        throw Error("Could not create buffer in WebGL 2 context.");
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, a_positionBuf);
    gl.bufferData(gl.ARRAY_BUFFER, VERTS, gl.STATIC_DRAW);

    const vao = gl.createVertexArray();
    if (vao === null) {
        throw Error("Could not create VAO in WebGL 2 context.");
    }
    gl.bindVertexArray(vao);
    gl.enableVertexAttribArray(a_positionLoc);
    gl.vertexAttribPointer(a_positionLoc, 2, gl.FLOAT, false, 0, 0);

    return new GraphicsState(
        gl,
        new GraphicsProgram(
            prog,
            vao,
            new Bufs(a_positionBuf),
            new Unifs(u_resolutionLoc, u_translationLoc, u_rotationLoc),
        ),
    );
}
