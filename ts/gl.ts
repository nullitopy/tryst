export const enum ShaderType {
    Vertex,
    Fragment,
}

/**
 * @example
 * ```ts
 * console.assert(shaderTypeString(ShaderType.Vertex) === "vertex");
 * ```
 */
export function shaderTypeString(shaderType: ShaderType): string {
    switch (shaderType) {
        case ShaderType.Vertex:
            return "vertex";
        case ShaderType.Fragment:
            return "fragment";
    }
}

function createShader(
    gl: WebGL2RenderingContext,
    type: ShaderType,
    source: string,
): WebGLShader {
    const shader = gl.createShader(
        (() => {
            switch (type) {
                case ShaderType.Vertex:
                    return gl.VERTEX_SHADER;
                case ShaderType.Fragment:
                    return gl.FRAGMENT_SHADER;
            }
        })(),
    );
    if (shader === null) {
        throw Error("Could not create shader in WebGL 2 context.");
    }

    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        return shader;
    }

    const log = gl.getShaderInfoLog(shader);
    gl.deleteShader(shader);
    throw Error(
        `Failed to compile WebGL 2 ${shaderTypeString(
            type,
        )} shader.\n\n${log}`,
    );
}

function createProgramInternal(
    gl: WebGL2RenderingContext,
    vertShader: WebGLShader,
    fragShader: WebGLShader,
): WebGLProgram {
    const program = gl.createProgram();
    if (program === null) {
        throw Error("Could not create program in WebGL 2 context.");
    }

    gl.attachShader(program, vertShader);
    gl.attachShader(program, fragShader);
    gl.linkProgram(program);
    if (gl.getProgramParameter(program, gl.LINK_STATUS)) {
        return program;
    }

    const log = gl.getProgramInfoLog(program);
    gl.deleteProgram(program);
    throw Error(`Failed to link WebGL 2 program.\n\n${log}`);
}

export function createProgram(
    gl: WebGL2RenderingContext,
    vertSource: string,
    fragSource: string,
): WebGLProgram {
    return createProgramInternal(
        gl,
        createShader(gl, ShaderType.Vertex, vertSource),
        createShader(gl, ShaderType.Fragment, fragSource),
    );
}
