export const JUMP_INTERVAL = 1_000.0;

export const enum AbstractKey {
    Left = 0b001,
    Right = 0b010,
    Jump = 0b100,
}

function codeToAbstract(code: string): AbstractKey | -1 {
    switch (code) {
        case "KeyA":
        case "ArrowLeft":
            return AbstractKey.Left;
        case "KeyD":
        case "ArrowRight":
            return AbstractKey.Right;
        case "Space":
            return AbstractKey.Jump;
    }

    return -1;
}

export function startListening(): () => number {
    let keyboardState = 0b000;

    document.addEventListener("keydown", e => {
        if (e.defaultPrevented) {
            return;
        }

        const abstractKey = codeToAbstract(e.code);
        if (abstractKey !== -1) {
            keyboardState |= abstractKey;
            e.preventDefault();
        }
    });

    document.addEventListener("keyup", e => {
        if (e.defaultPrevented) {
            return;
        }

        const abstractKey = codeToAbstract(e.code);
        if (abstractKey !== -1) {
            keyboardState &= ~abstractKey;
            e.preventDefault();
        }
    });

    return () => keyboardState;
}
